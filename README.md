# DiagArchi 

(Philippe Verley 2018-02-09: readme file deprecated, must be updated)

Copyright CIRAD-CNPF-INRA 2016

Version 2016-10-28

## Présentation

Application mobile de diagnostic architectural des arbres

## Auteurs

### Données

- **CIRAD**
  - Yves Caraglio
  - Eric Nicolini
  - Sylvie Annabel Sabatier
- **CNPF**
  - Christophe Drenou

### Développement

- **INRA**
  - Samuel Dufour-Kowalski 
  - Gaetan Perris 
  - Maeva Rouxel 

## Dépendances

- **Logicelles**
  - Apache Cordova + plugins (licence Apache 2)
  - Angular-JS (licence MIT)
  - Leaflet (licence BSD)
  - Boostrap (licence MIT)
  - Swiper (licence MIT)
  - D3 (licence BSD)
- **Compilation**
  - Java >= 1.7
  - Node.JS
  - Android SDK

## Compilation

```bash
npm install -g cordova
chmod a+x ./hooks/after_prepare/copy_icons.js
cordova platform update android
cordova build android
```
