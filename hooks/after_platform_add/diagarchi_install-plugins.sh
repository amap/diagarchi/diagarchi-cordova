#!/bin/bash 
# This hook installs all your plugins
 
# add your plugins to this list
declare -a plugins=(
    "cordova-plugin-camera" 
    "cordova-plugin-dialogs"
    "cordova-plugin-email"
    "cordova-plugin-file"
    "cordova-plugin-geolocation"
    "cordova-plugin-whitelist"
    )

for plugin in "${plugins[@]}"
do
	echo "cordova plugin add $plugin"
	cordova plugin add $plugin
done   
