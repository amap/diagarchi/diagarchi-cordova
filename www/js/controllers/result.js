/*
 * Ce fichier sert au différentes fonction utiliser dans result.html
 */
"use strict";

var diagArchi = angular.module('diagArchi');
diagArchi.controller('ResultController', function ($scope, $location, $route, Tree) {
    var currentQuestionNb = $route.current.params.nb;
    $scope.currentTree = Tree;
    $scope.currentTree.history = JSON.parse($route.current.params.history);
    $scope.currentTree.cheminement = JSON.parse($route.current.params.cheminement);

    // Populate the view with necessary data.
    // On recupere le typ (sa, r....)
    $scope.type = $scope.currentTree.data[currentQuestionNb].type;
    // Son etat (sain, résilient ....)
    $scope.etat = $scope.currentTree.data[currentQuestionNb].etat;
    $scope.description = $scope.currentTree.data[currentQuestionNb].description;
    $scope.definition = $scope.currentTree.data[currentQuestionNb].definition;
    
    $scope.images = [];

    if ($scope.currentTree.data[currentQuestionNb].hasOwnProperty("images")) {
        for (var i = 0; i < $scope.currentTree.data[currentQuestionNb].images.length; i++)
        {
            $scope.images.push('arbres/' + $scope.currentTree.type + '/' + $scope.currentTree.data[currentQuestionNb].images[i]);
        }
    }
    if ($scope.currentTree.data[currentQuestionNb].hasOwnProperty("imageEtat")
            && $scope.currentTree.data[currentQuestionNb].imageEtat.length > 0) {
        $scope.images.push('arbres/' + $scope.currentTree.type + '/' + $scope.currentTree.data[currentQuestionNb].imageEtat[0]);
    }
    
    $scope.currentTree.r_type = $scope.currentTree.data[currentQuestionNb].type;

    // Fonction pour aller a la sauvegarde de l'arbre
    $scope.goToSave = function () {
        $scope.currentTree.history.push(currentQuestionNb);
        $scope.currentTree.goToSave();
    };
});
