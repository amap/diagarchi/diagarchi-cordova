/*
 * Ce fichier sert au différentes fonction utiliser dans la page d'import/export: csv.html
 */
"use strict";

var diagArchi = angular.module('diagArchi');
diagArchi.controller('CsvController', function ($scope, $http, $location, Tree, CSV) {

    $scope.results = JSON.parse(localStorage.getItem("results")) || [];

    // Variable qui servira a compter le nombre de photo supprimer
    var compteur = 0;
    // Variable tableau qui contiendra les photos a supprimer.
    var listPhoto_supr = [];

    // Fonction appelée lors du clic sur exporter.
    $scope.exportCSV = function () {
        function onPrompt(filename) {
            if (filename !== null && filename.input1.trim() !== '') {
                // Ici on fait appelle au cervice CSV.
                CSV.export(filename.input1);
            } else {
                alert('Vous n\'avez pas entré un nom valide');
            }
        }

        navigator.notification.prompt(
                'Entrez le nom du fichier que vous allez exporter sans extension.',
                onPrompt,
                'Export',
                ['OK', 'Annuler']
                );
    };

    // Fonction appelée lors du clic sur importer.
    $scope.importCSV = function () {
        function onPrompt(filename) {
            if (filename !== null && filename.input1.trim() !== '') {
                // Ici on fait appelle au service CSV.
                CSV.import(filename.input1, function (array) {
                    $scope.$apply(function () {
                        localStorage.setItem("results", JSON.stringify(array));
                        alert("Observations importées");
                    });
                });
            } else {
                alert('Vous n\'avez pas entré un nom valide');
            }
        }

        navigator.notification.prompt(
                'Entrez le nom du fichier à importer sans extension. (Le fichier doît être à la racine du téléphone)',
                onPrompt,
                'Import',
                ['OK', 'Annuler']
                );
    };

    //Fonction appelée lors du clic sur le bouton envoyer csv
    $scope.sendCSV = function () {
        
        function onPrompt(filename) {
            if (filename !== null && filename.input1.trim() !== '') {
                // Envoi de mail
                var email = {
                    to: 'diagArchi@gmail.com',
                    attachments: [
                      cordova.file.externalRootDirectory+filename.input1+'.csv'
                    ],
                    subject: 'CSV DiagARCHI',
                    body: 'CSV in  attached document.',
                    isHtml: true
                  };
                  
                cordova.plugins.email.isAvailable(
                    function (isAvailable) {
                        if(isAvailable)
                        {
                            cordova.plugins.email.open(email, function () {
                                //vue fermée
                                 },this);
                         }
                             else alert('Echec de l\'envoi : service de mail indisponible');
                    }
                );
                 
                 
            } else {
                alert('Echec de l\'envoi : nom de fichier vide');
            }
        }

        navigator.notification.prompt(
                'Nom du CSV à envoyer :',
                onPrompt,
                'Envoi par mail',
                ['OK', 'Annuler']
                );
    };
    
 /*   function getCSVName()
    {
        function onPrompt(filename) {
            if (filename !== null && filename.input1.trim() !== '') {
                // Ici on fait appelle au cervice CSV.
                CSV.export(filename.input1);
                return filename.input1;
            } else {
                alert('Vous n\'avez pas entré un nom valide');
            }
        }

        navigator.notification.prompt(
                'Entrez le nom du fichier que vous allez exporter sans extension.',
                onPrompt,
                'Export',
                ['OK', 'Annuler']
                );
        
    }*/

    // Fonction appeller quand on clic sur supprimer.
    $scope.removeTrees = function () {
        function onConfirm(confirmation) {
            if (confirmation === 1) {
                if ($scope.results.length !== 0) {

                    // On remplit notre tableau de photo a supprimer.
                    for (var i = 0; i < $scope.results.length; i++) {
                        for (var j = 0; j < $scope.results[i].listPhoto.length; j++) {
                            listPhoto_supr.push($scope.results[i].listPhoto[j].url);
                        }
                    }
                    if (listPhoto_supr.length !== 0) {
                        // On parcoure le tableau et on supprime les fichiers
                        for (var i = 0; i < listPhoto_supr.length; i++) {
                            window.resolveLocalFileSystemURL(listPhoto_supr[i], resolveOnSuccess, resOnError1);
                        }
                        localStorage.removeItem("results");
                        // On supprime les données de rappel quand on efface les observation.
                        localStorage.removeItem("currentIdent");
                    } else {
                        localStorage.removeItem("results");
                        localStorage.removeItem("currentIdent");
                        alert("Observations supprimées");
                    }
                } else {
                    localStorage.removeItem("results");
                    localStorage.removeItem("currentIdent");
                    localStorage.removeItem("results_bis");
                    alert("Observations supprimées");
                }
            }
        }

        navigator.notification.confirm(
                'Supprimer toutes les observations ?',
                onConfirm,
                'Suppression',
                ['OK', 'Annuler']
                );
    };

    // Fonction appellée si on a trouvé un fichier.
    function resolveOnSuccess(entry) {
        // On supprime les fichier
        entry.remove(succes, resOnError2);
    }

    // Fonction appeller en cas de suppression reussit du fichier.
    function succes() {
        compteur++;
        // Quand la suppression de toutes les photo est fini on affiche un message.
        if (compteur === listPhoto_supr.length) {
            // ON verrifie que le dossier qui contient les photos est vide.
            dossierVide();
            alert("Observations supprimées");
        }
    }

    // Fonction qui verifie si le dossier est vide et le supprime le cas échéant.
    function dossierVide() {
        window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, function (fileSys) {
            // On cible le dossier
            fileSys.getDirectory('DiagArchi',
                    {create: false, exclusive: false},
            function (directory) {
                // En cas de success on va lire notre dossier
                var directRead = directory.createReader();
                directRead.readEntries(successRead, resOnError3);
            },
                    resOnError4);
        },
                resOnError5);
    }

    // Fonction appeller quand on lit le dossier
    function successRead(entries) {
        // SI le dossier est vide
        if (entries.length === 0) {
            window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, function (fileSys) {
                // On cible le dossier
                fileSys.getDirectory('DiagArchi',
                        {create: false, exclusive: false},
                function (directory) {
                    // Et on le supprime
                    directory.remove(succesDir, resOnError6);
                },
                        resOnError4);
            },
                    resOnError5);
        }
    }

    // Fonction appeller quand on supprime le dossier. Ici elle est vide.
    function succesDir() {
    }

    // Differente fonction d'erreur pour ptenir le code et l'erreur.
    // NB on aurais put utiliser une seule fonction.
    function resOnError1(error) {
        alert('Erreur de fichier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError2(error) {
        alert('Erreur de suppression du fichier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError3(error) {
        alert('Erreur de lecture du dossier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError4(error) {
        alert('Erreur de dossier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError5(error) {
        alert('Erreur de requete de lecture de fichier, code:  ' + error.code + ' message : ' + error.message);
    }

    function resOnError6(error) {
        alert('Erreur de suppression du dossier, code : ' + error.code + ' message : ' + error.message);
    }
});
