/*
 * Ce fichier sert au différentes fonction de la géolocalisation geolocalisation.html.
 */
"use strict";

var diagArchi = angular.module('diagArchi');
diagArchi.controller('GeolocalisationController', function ($scope, $http, $location, Tree, CSV, $compile) {

    // Une varibale map qui contiendra la carte
    var map = null;
    // Variable qui servira de marqueur
    var marker;
    // Variable pour les information sur la carte
    var infowindow;

    // Variable pour la latitude (utilie dans le cas ou on veut voir la position de l'arbre)
    var latitude = $location.search().latitude || null;
    // Variable pour la longitude (utilie dans le cas ou on veut voir la position de l'arbre)
    var longitude = $location.search().longitude || null;
    // Variable pour voir les arbres du codePlacette.
    var codePlacette = $location.search().cp || null;
    var results_bis = JSON.parse(localStorage.getItem("results_bis")) || null;

    // Fonction pour afficher notre position en absence de reseaux.
    function onSuccess_nogps(position) {
        longitude = position.coords.longitude;
        latitude = position.coords.latitude;
        map.remove();
        var map2 = L.map('map').setView([latitude, longitude], 18);
        L.tileLayer('map/MapQuest/{z}/{x}/{y}.png', {id: ''}).addTo(map2);
        L.marker([latitude, longitude]).addTo(map2);
    }

    function onError_nogps(error) {
        alert('Les coordonnées n\'ont pas pus être récupérées.' + '\n' +
                'Error: ' + error.code + ' - ' + error.message);
    }

    //Quand la page a fini de ce charger on lance le listener qui va afficher la carte.
    //$(document).ready(function () {
        // ici on verifie si on est hors connection : google === undefinied ou connecter
        if (typeof google === 'undefined') {
            // On fixe la hauteur de l'affichage de la carte
            $(window).unbind();
            $(window).bind('pageshow resize orientationchange', function (e) {
                max_height();
            });
            max_height();
            // On centre la carte sur la france.
            map = L.map('map').setView([46.7562695, 0.9061153], 6);
            L.tileLayer('map/MapQuest/{z}/{x}/{y}.png', {id: ''}).addTo(map);
            // Cas de la position de l'arbre
            if (latitude !== null) {
                // On enleve la precedente carte
                map.remove();
                // Puis on en crée une nouvelle centrer sur l'arbre
                var map1 = L.map('map').setView([latitude, longitude], 18);
                L.tileLayer('map/MapQuest/{z}/{x}/{y}.png', {id: ''}).addTo(map1);
                L.marker([latitude, longitude]).addTo(map1);
            } else {
                // Sinon on affiche notre position actuel
                navigator.geolocation.getCurrentPosition(onSuccess_nogps, onError_nogps, {enableHighAccuracy: true});
            }
        } else {
            document.addEventListener("deviceready", onDeviceReady, false);
        }
    //});

    // On modifie la taille de la fenêtre.
    function onDeviceReady() {
        $(window).unbind();
        $(window).bind('pageshow resize orientationchange', function (e) {
            max_height();
        });
        max_height();
        google.load("maps", "3", {"callback": map_gps, other_params: "language=fr"});
    }

    // Fonction qui calcule la taille de la carte en fonction de l'ecran.
    function max_height() {
        var h = $('div[data-role="header"]').outerHeight(true);
        var f = $('div[data-role="footer"]').outerHeight(true);
        var w = $(window).height();
        var c = $('div[data-role="content"]');
        var c_h = c.height();
        var c_oh = c.outerHeight(true);
        var c_new = w - h - f - c_oh + c_h;
        if (c_h < c[0].scrollHeight) {
            c.height(c[0].scrollHeight);
        } else {
            c.height(c_new);
        }
    }

    // affiche la carte centrer sur la france puis sur notre position ou celle de l'arbre.
    function map_gps() {
        var latlng = new google.maps.LatLng(46.7562695, 0.9061153);
        var myOptions = {
            zoom: 6,
            center: latlng,
            streetViewControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: true
        };

        // Ici on affiche une premiere fois la carte centrer sur la france
        map = new google.maps.Map(document.getElementById("map"), myOptions);

        // Ici on centre la carte sur ce que l'on souhaite
        google.maps.event.addListenerOnce(map, 'tilesloaded', function () {
            navigator.geolocation.getCurrentPosition(onSuccess, onError, {enableHighAccuracy: true});
        });
    }

    // Centre la carte sur nous ou sur l'arbre, ajoute un marqueur.
    function onSuccess(position) {
        if (codePlacette !== null) {
            if (results_bis !== null && results_bis[codePlacette]['coord']) {
                // Variable pour savoir si on a generer la map
                var map_bon = false;
                for (var i = 0; i < results_bis[codePlacette]['donnee'].length; i++) {
                    if (results_bis[codePlacette]['donnee'][i].latitude !== null) {
                        if (map_bon) {
                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(results_bis[codePlacette]['donnee'][i].latitude, results_bis[codePlacette]['donnee'][i].longitude),
                                map: map
                            });

                            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                return function () {
                                    infowindow.setContent('arbre : ' + results_bis[codePlacette]['donnee'][i].numArbre
                                            + '<br> latitude : ' + results_bis[codePlacette]['donnee'][i].latitude
                                            + '<br> longitude : ' + results_bis[codePlacette]['donnee'][i].longitude);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                        } else {
                            var latlng = new google.maps.LatLng(results_bis[codePlacette]['donnee'][i].latitude, results_bis[codePlacette]['donnee'][i].longitude);
                            var myOptions = {
                                zoom: 15,
                                center: latlng,
                                streetViewControl: true,
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                zoomControl: true
                            };

                            map = new google.maps.Map(document.getElementById("map"), myOptions);

                            infowindow = new google.maps.InfoWindow();

                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(results_bis[codePlacette]['donnee'][i].latitude, results_bis[codePlacette]['donnee'][i].longitude),
                                map: map
                            });

                            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                return function () {
                                    infowindow.setContent('arbre : ' + results_bis[codePlacette]['donnee'][i].numArbre
                                            + '<br> latitude : ' + results_bis[codePlacette]['donnee'][i].latitude
                                            + '<br> longitude : ' + results_bis[codePlacette]['donnee'][i].longitude);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));

                            map_bon = true;
                        }
                    }
                }
            } else {
                alert('bob');
            }
        } else {
            if (latitude === null) {
                longitude = position.coords.longitude;
                latitude = position.coords.latitude;
            }
            var latlng = new google.maps.LatLng(latitude, longitude);
            var myOptions = {
                zoom: 15,
                center: latlng,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: true
            };

            map = new google.maps.Map(document.getElementById("map"), myOptions);

            var info =
                    ('Latitude: ' + latitude + '<br>' +
                            'Longitude: ' + longitude + '<br>');

            if (!marker) {
                //creer le marqueur
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map
                });
            } else {
                //deplace le marqueur sur la nouvelle position
                marker.setPosition(latlng);
            }
            if (!infowindow) {
                infowindow = new google.maps.InfoWindow({
                    content: info
                });
            } else {
                infowindow.setContent(info);
            }
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
        }
    }

    // En cas d'erreur
    function onError(error) {
        alert('Erreur sur la géolocalisation\n code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
    }
});
