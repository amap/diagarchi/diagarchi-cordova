/*
 * Ce fichier sert au différentes fonction utiliser dans save.html
 */
"use strict";
var diagArchi = angular.module('diagArchi');
diagArchi.controller('SaveController', function ($scope, $location, $route, Tree) {

    // Variable pour les photo prise avec l'appareil photo.
    var photo_tmp = [];
    // Compteur qui servira a verifier qu'on a bien deplacer toute les photos
    var photo_compteur = 0;
    // Variable pour le cas ou on ajoute des photo lors de la modification d'un arbre.
    var photo_update = [];
    // Variable pour verifier si l'observation existe
    var obsExist = false;
    // Variable pour indiquer si'il y a eu modification du numéro et/ou du code placette
    var modif = false;
    // Compteur qui servira a verifier qu'on a bien renomer toutes les photos lors de la modification d'une observation.
    var photo_old_compteur = 0;
    // Variable qui contiendra les photo pour modification dans le cas de la modification d'une observation.
    var photo_old = [];
    // Variable qu'on remplira avec les photo renomer
    var photo_rename = [];
    $scope.currentTree = Tree;
    $scope.currentTree.history = JSON.parse($route.current.params.history);
    // On recuperer les donner precedament rentrer
    $scope.currentIdent = JSON.parse(localStorage.getItem("currentIdent")) || null

    // Variable pour garder l'ancien numéro d'arbre
    var oldNum = Tree.numArbre;
    // Variable pour garder l'ancien code Placette
    var oldPlacette = Tree.codePlacette;
    if ($scope.currentTree.isBeingUpdated) {

    } else {
        if ($scope.currentIdent !== null) {
            //On affecte ces données au champ correspondant pour quelle soit afficher dans la page.
            $scope.currentTree.observateur = $scope.currentIdent['observateur'];
            $scope.currentTree.secondObs = $scope.currentIdent['secondObs'];
            $scope.currentTree.codePlacette = $scope.currentIdent['codePlacette'];
        }
    }


    //Fonction pour sauvegarder les données.
    $scope.save = function () {

        // Correction d'un bug (ajout de !== null pour verifier que les champ ne sont pas vide
        if (typeof $scope.currentTree.observateur !== "undefined" &&
                typeof $scope.currentTree.numArbre !== "undefined" &&
                typeof $scope.currentTree.codePlacette !== "undefined" &&
                $scope.currentTree.observateur !== null &&
                $scope.currentTree.numArbre !== null &&
                $scope.currentTree.codePlacette !== null) {

            var results = JSON.parse(localStorage.getItem("results")) || [];
            // Variable qui servira a recuperer l'arbre existant en cas de modif.
            var treeThatAlreadyExist = null;
            for (var i = 0; i < results.length; i++) {
                if (results[i].numArbre === $scope.currentTree.numArbre && results[i].codePlacette === $scope.currentTree.codePlacette) {
                    treeThatAlreadyExist = results[i];
                    break;
                }
            }

            var message;
            //Ici on cree un nouveau objet date
            var d = new Date();
            if ($scope.currentTree.isBeingUpdated) {
                for (var i = 0; i < results.length; i++) {
                    if (results[i].numArbre === oldNum && results[i].codePlacette === oldPlacette) {
                        results[i].observateur = $scope.currentTree.observateur;
                        // On transforme le timestamp en chaine de craractere.
                        // results[i].dateObservation = d.toLocaleDateString();
                        if ($scope.currentTree.longitude !== '') {
                            results[i].longitude = $scope.currentTree.longitude;
                        }
                        if ($scope.currentTree.latitude !== '') {
                            results[i].latitude = $scope.currentTree.latitude;
                        }
                        results[i].historique = $scope.currentTree.history;
                        // Ajout de l'objet cheminement (chaine de reponse) dans l'export
                        results[i].cheminement = $scope.currentTree.cheminement;
                        // Ajout du type dans l'export.
                        results[i].r_type = $scope.currentTree.r_type;
                        results[i].version = $scope.currentTree.version;

                        if (results[i].numArbre !== $scope.currentTree.numArbre || results[i].codePlacette !== $scope.currentTree.codePlacette) {
                            modif = true;
                            for (var j = 0; j < results.length; j++) {
                                if (results[j].numArbre === $scope.currentTree.numArbre && results[j].codePlacette === $scope.currentTree.codePlacette) {
                                    obsExist = true;
                                    message = "Une observation portant ce numéro et ce code placette existe déjà. Modifiez ces numéros ou l'observation existante.";
                                    break;
                                }
                            }
                        }
                        if (!obsExist) {
                            message = "Observation modifiée";
                            results[i].numArbre = $scope.currentTree.numArbre;
                            results[i].codePlacette = $scope.currentTree.codePlacette;
                        }
                        break;
                    }
                }

            } else {
                if (treeThatAlreadyExist === null) {
                    results.push({
                        observateur: $scope.currentTree.observateur,
                        secondObs: $scope.currentTree.secondObs,
                        dateObservation: d.toLocaleDateString(),
                        numArbre: $scope.currentTree.numArbre,
                        essence: $scope.currentTree.type,
                        codePlacette: $scope.currentTree.codePlacette,
                        longitude: $scope.currentTree.longitude,
                        latitude: $scope.currentTree.latitude,
                        historique: $scope.currentTree.history,
                        cheminement: $scope.currentTree.cheminement,
                        listPhoto: $scope.currentTree.listPhoto,
                        r_type: $scope.currentTree.r_type,
                        version: $scope.currentTree.version
                    });
                    message = "Observation ajoutée";
                    obsExist = false;
                    // Enregistrement des données pour futurs enregistrements
                    var currentIdent = new Object();
                    currentIdent['observateur'] = $scope.currentTree.observateur;
                    currentIdent['secondObs'] = $scope.currentTree.secondObs;
                    currentIdent['codePlacette'] = $scope.currentTree.codePlacette;
                    localStorage.setItem("currentIdent", JSON.stringify(currentIdent));
                } else {
                    message = "Une observation portant ce numéro et ce code placette existe déjà. Modifiez ces numéros ou l'observation existante.";
                    obsExist = true;
                }
            }
            alert(message);
            if (!obsExist) {
                localStorage.setItem("results", JSON.stringify(results));
                // On verifie qu'on a pris des photos.
                if (photo_tmp.length !== 0) {
                    // On parcoure le tableau et on lance le transfer des photos via window.resolveLocalFileSystemURL
                    for (var i = 0; i < photo_tmp.length; i++) {
                        window.resolveLocalFileSystemURL(photo_tmp[i], resolveOnSuccess, resOnError1);
                    }
                } else {
                    if (modif) {
                        photo_old = JSON.parse($scope.currentTree.listPhoto);
                        if (photo_old.length !== 0) {
                            for (var i = 0; i < photo_old.length; i++) {
                                window.resolveLocalFileSystemURL(photo_old[i].url, resolveOnRename, resOnError1);
                            }
                        } else {
                            $location.url('/observations');
                        }
                    } else {
                        $location.url('/observations');
                    }
                }
            } else {
                obsExist = false;
            }
        } else {
            alert("Tous les champs ne sont pas remplis.");
        }
    };
    // Fonction pour avoir la position gps.
    $scope.getPosition = function () {
        function onSuccess(position) {
            $scope.$apply(function () {
                $scope.currentTree.longitude = position.coords.longitude;
                $scope.currentTree.latitude = position.coords.latitude;
            });
            navigator.notification.alert(
                    'Les coordonnées ont été ajoutées.',
                    null,
                    'Fin de recherche de coordonnées',
                    'Ok');
        }

        function onError(error) {
            alert('Les coordonnées n\'ont pas pus être récupérées.' + '\n' +
                    'Erreur: ' + error.code + ' - ' + error.message);
        }

        navigator.geolocation.getCurrentPosition(onSuccess, onError, {enableHighAccuracy: true});
    };
    // Fonction pour la prise de photo
    $scope.photo = function () {
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URI,
            correctOrientation: true
        });
        // En cas de succes on ajoute la photo au tableau photo_tmp.
        function onSuccess(imageURI) {
            photo_tmp.push(imageURI);
        }

        function onFail(message) {
            alert('Echec de prise de photo: ' + message);
        }


    };
    // Fonction pour le transfer des photo du repertoire temporaire au repertoire DiagArchi.
    function resolveOnSuccess(entry) {
        var d = new Date();
        var n = d.getTime();
        //Nouveau nom de fichier.
        var newFileName = $scope.currentTree.codePlacette + '_' + $scope.currentTree.type + '_' + $scope.currentTree.numArbre + '_' + n + ".jpg";
        var myFolderApp = "DiagArchi";
        window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, function (fileSys) {
            //Le dossier est creer s'il n'existe pas
            fileSys.getDirectory(myFolderApp,
                    {create: true, exclusive: false},
                    function (directory) {
                        entry.moveTo(directory, newFileName, successMove, resOnError2);
                    },
                    resOnError3);
        },
                resOnError4);
    }

    // Fonction pour le renomage des photo en cas de modification de l'observation.
    function resolveOnRename(entry) {
        var d = new Date();
        var n = d.getTime();
        //Nouveau nom de fichier.
        var newFileName = $scope.currentTree.codePlacette + '_' + $scope.currentTree.type + '_' + $scope.currentTree.numArbre + '_' + n + ".jpg";
        var myFolderApp = "DiagArchi";
        window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, function (fileSys) {
            //Le dossier est creer s'il n'existe pas
            fileSys.getDirectory(myFolderApp,
                    {create: true, exclusive: false},
                    function (directory) {
                        entry.moveTo(directory, newFileName, successMove2, resOnError2);
                    },
                    resOnError3);
        },
                resOnError4);
    }

    // Fonction appellée en cas du succés du transfert.
    function successMove(entry) {

        // On recupere notre objet results
        var results = JSON.parse(localStorage.getItem("results"));
        // On creer un nouveau tableau vide
        var listPhoto_tree = [];
        // Si on a deja des photos pour cette arbre alors on reafecte ces photos a notre nouveau tableau.
        if ($scope.currentTree.listPhoto.length !== 0) {
            listPhoto_tree = JSON.parse($scope.currentTree.listPhoto);
        }

        // Si on a ajouté une photo lors de la modification d'un arbre et que celui ci en contient deja alors on remplis notre tableau photo_update
        if (photo_update.length === 0 && listPhoto_tree.length !== 0) {
            for (var i = 0; i < listPhoto_tree.length; i++) {
                photo_update.push(listPhoto_tree[i]);
            }

            // Puis on ajoute la nouvelle photo
            photo_update.push({
                url: entry.nativeURL,
                name: entry.name
            });
            photo_compteur++;
        } else {
            // Si on a pris plusieur photos pendant la modification
            if (photo_update.length !== 0) {
                photo_update.push({
                    url: entry.nativeURL,
                    name: entry.name
                });
                photo_compteur++;
            } else {
                // Cas ou on est dans la creation de l'observation ou dans la modification sans photo prealable.
                for (var i = 0; i < results.length; i++) {
                    if (results[i].numArbre === $scope.currentTree.numArbre && results[i].codePlacette === $scope.currentTree.codePlacette) {
                        results[i].listPhoto.push({
                            url: entry.nativeURL,
                            name: entry.name
                        });
                        localStorage.setItem("results", JSON.stringify(results));
                        photo_compteur++;
                        break;
                    }
                }
            }
        }
        // Quand on a fini de traiter toutes les photos prise pars l'utilisateur pour l'observation
        if (photo_compteur === photo_tmp.length) {
            // Cas de la modification
            if (photo_update.length !== 0) {
                for (var i = 0; i < results.length; i++) {
                    if (results[i].numArbre === $scope.currentTree.numArbre && results[i].codePlacette === $scope.currentTree.codePlacette) {
                        // Si le numéro d'arbre ou le code placette a changé.
                        if (modif) {
                            photo_old = photo_update;
                            for (var i = 0; i < photo_old.length; i++) {
                                window.resolveLocalFileSystemURL(photo_old[i].url, resolveOnRename, resOnError1);
                            }
                        } else {
                            results[i].listPhoto = photo_update;
                            localStorage.setItem("results", JSON.stringify(results));
                            $scope.$apply(function () {
                                $location.url('/observations/');
                            });
                        }
                    }
                }
            } else {
                // On retourne sur l'index
                $scope.$apply(function () {
                    $location.url('/observations');
                });
            }

        }
    }

    function successMove2(entry) {
        // On recupere notre objet results
        var results = JSON.parse(localStorage.getItem("results"));
        photo_rename.push({
            url: entry.nativeURL,
            name: entry.name
        });
        photo_old_compteur++;
        // Quand on a fini de traiter toutes les photo a deplacer
        if (photo_old_compteur === photo_old.length) {
            for (var i = 0; i < results.length; i++) {
                if (results[i].numArbre === $scope.currentTree.numArbre && results[i].codePlacette === $scope.currentTree.codePlacette) {
                    results[i].listPhoto = photo_rename;
                    localStorage.setItem("results", JSON.stringify(results));
                    break;
                }
            }
            // On retourne sur l'index
            $scope.$apply(function () {
                $location.url('/observations');
            });
        }
    }

    function resOnError1(error) {
        alert('Erreur de fichier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError2(error) {
        alert('Erreur de deplacement du fichier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError3(error) {
        alert('Erreur de dossier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError4(error) {
        alert('Erreur de requete de lecture de fichier, code:  ' + error.code + ' message : ' + error.message);
    }
});
