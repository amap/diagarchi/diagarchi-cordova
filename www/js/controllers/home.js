/*
 * Ce fichier sert au différentes fonction utiliser dans home.html
 */
"use strict";

var diagArchi = angular.module('diagArchi');

diagArchi.controller('HomeController', function ($scope, $location, Tree, CSV, Modal) {
    
    $scope.Modal = Modal;
    
    Modal.getGlossaire();

});