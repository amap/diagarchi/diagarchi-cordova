/*
 * Ce fichier sert au différentes fonction utiliser dans question.html
 */
"use strict";
var diagArchi2 = angular.module('diagArchi');
diagArchi2.controller('QuestionController', function ($scope, $http, $location, $route, Tree, $sce, Modal) {
    
    $scope.Modal = Modal;
    
    var currentQuestionNb = $route.current.params.nb;
    $scope.currentTree = Tree;
    $scope.currentTree.history = JSON.parse($route.current.params.history);
    $scope.currentTree.cheminement = JSON.parse($route.current.params.cheminement);

    // Variable pour le glossaire
    var glossaire = JSON.parse(localStorage.getItem("glossaire"));
    // Variable qui servira a verifier qu'on a deja utiliser ce mot dans une question
    var glossaire_bis = new Array();

    // Ajout de la position pour montrer quoi regarder.
    if ($scope.currentTree.data[currentQuestionNb].hasOwnProperty("position")) {
        $scope.position = 'arbres/' + $scope.currentTree.type + '/' + $scope.currentTree.data[currentQuestionNb].position;
    }

    traitementQuestion();

    $scope.images = null;

    // Ici on verifie si la question a des images.
    if ($scope.currentTree.data[currentQuestionNb].hasOwnProperty("images")) {

        $scope.images = [];
        $scope.isArray = angular.isArray;

        for (var i = 0; i < $scope.currentTree.data[currentQuestionNb].images.length; i++) {
            var currentImage = $scope.currentTree.data[currentQuestionNb].images[i];
            $scope.images.push('arbres/' + $scope.currentTree.type + '/' + currentImage);
        }
    }
    

    // Fonction pour aller a la question suivante.
    $scope.reponse = function (rep) {
        $scope.currentTree.history.push(currentQuestionNb);
        // On remplis notre objet cheminement.
        if (rep === "oui") {
            $scope.currentTree.cheminement.push("1");
        } else {
            if (rep === "non") {
                $scope.currentTree.cheminement.push("0");
            }
        }

        $scope.currentTree.goToNextQuestion(rep);
    };

    function traitementQuestion() {
        var question = $scope.currentTree.data[currentQuestionNb].question;
        //Tableau de mots
        var mots = question.split(" ");
        //Liste de mots avec la propriété glossaire ou pas
        var questionFinal = new Array();

        var i = 0;
        //Pour chaque mot
        while (i < mots.length) {
            // Variable tableau qui contiendra les différent resultat au court d'une boucle. 
            // Utilie dans le cas ou 2 mot qui se suive aurais une definition separer et commune (exemple houppier HC)
            var glossaire_tab = new Array();
            // Variable qui contiendra la phrase en court a partir de i.
            var expression;
            for (var j = i; j < mots.length; j++) {
                if (j === i) {
                    expression = mots[i];
                } else {
                    expression += ' ' + mots[j];
                }

                var formattedExpression = expression.charAt(0).toUpperCase() 
                        + expression.substr(1);
                
                if (typeof glossaire[formattedExpression] !== "undefined") {
                    glossaire_tab.push({
                        mot: expression,
                        motGlossaire: formattedExpression,
                        indice: j
                    });
                }
            }
            if (glossaire_tab.length > 0) {
                var present = false;
                for (var k = 0; k < glossaire_bis.length; k++) {
                    // On compare au dernier element du tableau
                    if (glossaire_bis[k] === glossaire_tab[glossaire_tab.length - 1].mot) {
                        present = true;
                        break;
                    }
                }
                if (!present) {
                    // On ajoute le mot associé a une variable boolean a true
                    //  si c'est la premiere fois qu'on rencontre ce mot
                    questionFinal.push({
                        mot: glossaire_tab[glossaire_tab.length - 1].mot,
                        motGlossaire: glossaire_tab[glossaire_tab.length - 1].motGlossaire,
                    });
                    // On ajoute ce mot a notre tableau glossaire_bis
                    glossaire_bis.push(glossaire_tab[glossaire_tab.length - 1].mot);
                } else {
                    // Cas ou le mot a deja etait associé au glossaire
                    questionFinal.push({
                        mot: glossaire_tab[glossaire_tab.length - 1].mot,
                        motGlossaire: null
                    });
                }
                i = glossaire_tab[glossaire_tab.length - 1].indice;

            } else {
                // Cas ou le mot n'a pas de correspondance dans le glossaire
                questionFinal.push({
                    mot: mots[i],
                    motGlossaire: null
                });
            }
            i++;
        }
        $scope.questions = questionFinal;
    }
});
