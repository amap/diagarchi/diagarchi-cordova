/*
 * Ce fichier sert a récupérerl a liste des photos d'un arbre pour les afficher.
 */
"use strict";

var diagArchi = angular.module('diagArchi');
diagArchi.controller('PhotoController', function ($scope, $location, $route, Tree, $http) {

    // On recuperer la variable listPhoto de l'url.
    var listPhoto = JSON.parse($location.search().listPhoto) || null;

    // On remplit notre tableau avec les url des photos
        if (listPhoto.length !== 0) {
            $scope.images = [];
            for (var i = 0; i < listPhoto.length; i++) {
                $scope.images.push(listPhoto[i].url);
            }
        }
        else {
            alert('ERREUR pas de photo');
            $location.url('/observations');
        }

    // Fonction utiliser quand on clic sur retour.
    $scope.retour = function () {
        $location.url('/observations');
    };

    function max_height() {
        var h = $('div[data-role="header"]').outerHeight(true);
        var f = $('div[data-role="footer"]').outerHeight(true);
        var w = $(window).height();
        var c = $('div[data-role="content"]');
        var c_h = c.height();
        var c_oh = c.outerHeight(true);
        var c_new = w - h - f - c_oh + c_h;
        var total = h + f + c_oh;
        if (c_h < c.get(0).scrollHeight) {
            c.height(c.get(0).scrollHeight);
        } else {
            c.height(c_new);
        }
    }
});
