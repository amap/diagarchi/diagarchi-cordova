/*
 * Ce fichier sert au différentes fonction utiliser dans home.html
 */
"use strict";

var diagArchi = angular.module('diagArchi');

diagArchi.controller('ObservationsController', function ($scope, $http, $location, Tree, CSV, Modal) {

    // Compteur servant a verifier qu'on a bien supprimer toutes les photos
    var compteur = 0;
    // La liste des photos a supprimer
    var listPhoto_supr = [];

    $scope.Modal = Modal;

    // Look for every species available.
    $http({method: 'GET', url: 'arbres/index.json'}).success(function (data, status, headers, config) {
        $scope.arbres = data;
    });
    

    $scope.results = JSON.parse(localStorage.getItem("results")) || [];

    afficheObservation($scope.results);

    // Fonction appellée quand on choisie un type d'arbre a diagnostiquer.
    $scope.checkTree = function (type, history) {
        $http({method: 'GET', url: 'arbres/' + type + '/index.json'}).success(function (data, status, headers, config) {
            $scope.currentTree = Tree;
            $scope.currentTree.newTree(data, type);
            $scope.currentTree.goToQuestion(0);
        });
    };

    // Fonction servant a la modification d'un arbre.
    $scope.checkTreeDone = function (type, history, observateur, numArbre, codePlacette, longitude, latitude, cheminement, listPhoto) {
        $http({method: 'GET', url: 'arbres/' + type + '/index.json'}).success(function (data, status, headers, config) {
            $scope.currentTree = Tree;
            history = JSON.parse(JSON.stringify(history));
            cheminement = JSON.parse(JSON.stringify(cheminement));
            $scope.currentTree.newTree(data, type, {observateur: observateur, numArbre: numArbre, codePlacette: codePlacette, longitude: longitude, latitude: latitude, history: history, cheminement: cheminement, listPhoto: listPhoto}, true);
            $scope.currentTree.goToResult();
        });
    };

    // Fonction servant a la suppression d'un arbre
    $scope.deleteTree = function (index) {
        var confirmation = confirm('Supprimer cette observation ?');
        if (confirmation) {
            // On verifie s'il y a des photo.
            if ($scope.results[index].listPhoto.length !== 0) {
                for (var i = 0; i < $scope.results[index].listPhoto.length; i++) {
                    // On cree un nouveau tableau avec les url des photo
                    listPhoto_supr.push($scope.results[index].listPhoto[i].url);
                }
                // On parcoure le tableau et on supprime les fichiers
                for (var i = 0; i < listPhoto_supr.length; i++) {
                    window.resolveLocalFileSystemURL(listPhoto_supr[i], resolveOnSuccess, resOnError1);
                }
                $scope.results.splice(index, 1);
                localStorage.setItem("results", JSON.stringify($scope.results));
                afficheObservation($scope.results);

            } else {
                $scope.results.splice(index, 1);
                localStorage.setItem("results", JSON.stringify($scope.results));
                afficheObservation($scope.results);

            }
        }
    };

    function afficheObservation(results) {
        var results_bis;
        if (results.length === 0) {
            results_bis = null;
            localStorage.removeItem("results_bis");
            $scope.results_bis = results_bis;
        } else {
            results_bis = new Object();
            for (var i = 0; i < results.length; i++) {
                var bon = false;
                for (var codePlacette in results_bis) {
                    if (codePlacette === results[i].codePlacette) {
                        results_bis[codePlacette]['donnee'].push(results[i]);
                        if (results[i].latitude !== null) {
                            results_bis[codePlacette]['coord'] = true;
                        }
                        bon = true;
                    }
                    results_bis[codePlacette]['donnee'].sort(function(a,b){
                        return a.numArbre-b.numArbre;
                    });
                         
                }
                if (!bon) {
                    results_bis[results[i].codePlacette] = new Object();
                    results_bis[results[i].codePlacette]['coord'] = false;
                    results_bis[results[i].codePlacette]['donnee'] = new Array();
                    results_bis[results[i].codePlacette]['donnee'].push(results[i]);
                }
            }
            localStorage.setItem("results_bis", JSON.stringify(results_bis));
            $scope.results_bis = results_bis;
        }
    }

    // Fonction appellée en cas de ciblage reussie sur le fichier.
    function resolveOnSuccess(entry) {
        // On supprime les fichier
        entry.remove(succes, resOnError2);
    }

    // Fonction appellée en cas de suppression reussie.
    function succes() {
        compteur++;
        // Quand la suppression de toutes les photos est finie on affiche un message.
        if (compteur === listPhoto_supr.length) {
            dossierVide();
            alert('suppression terminer');

        }
    }

    // Fonction qui sert a verifier que le dossier qui contient les photos est vide.
    function dossierVide() {
        window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, function (fileSys) {
            // ON cible le dossier
            fileSys.getDirectory('DiagArchi',
                    {create: false, exclusive: false},
            function (directory) {
                // Si le ciblage a reussit on lit les fichier du dossier
                var directRead = directory.createReader();
                directRead.readEntries(successRead, resOnError3);
            },
                    resOnError4);
        },
                resOnError5);
    }

    // En cas de lecture reussit dans le dossier
    function successRead(entries) {
        if (entries.length === 0) {
            window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, function (fileSys) {
                fileSys.getDirectory('DiagArchi',
                        {create: false, exclusive: false},
                function (directory) {
                    // Si le dossier est vide on le supprime
                    directory.remove(succesDir, resOnError6);
                },
                        resOnError4);
            },
                    resOnError5);
        }
    }

    function succesDir() {
    }

    function resOnError1(error) {
        alert('Erreur de fichier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError2(error) {
        alert('Erreur de suppression du fichier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError3(error) {
        alert('Erreur de lecture du dossier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError4(error) {
        alert('Erreur de dossier, code : ' + error.code + ' message : ' + error.message);
    }

    function resOnError5(error) {
        alert('Erreur de requete de lecture de fichier, code:  ' + error.code + ' message : ' + error.message);
    }

    function resOnError6(error) {
        alert('Erreur de suppression du dossier, code : ' + error.code + ' message : ' + error.message);
    }
});
