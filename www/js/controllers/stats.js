/*
 * Ce fichier sert a l'affichage des stat.
 */
"use strict";
var diagArchi = angular.module('diagArchi');
diagArchi.controller('StatsController', function ($scope, $location, $route, Tree, $http) {

    // On récupère la tableau results.
    var results = JSON.parse(localStorage.getItem("results")) || [];
    // On compte le nombre total d'arbres 
    $scope.treeTotal = results.length;
    // On cree une nouvelle variable qui servira pour trier les different resultats du tableau results.
    var results_bis = new Object();
    // On cree un tableau qui contiendra les différentes essences pour récupérer les fichier correspondant.
    var tab_essence = new Array();
    // Variable pour les statistiques globale
    var total;
    // Variable boolean pour savoir si on a deja passer le codePlacette total
    var total_present = false;
    // Variable pour stoquer la div total;
    var total_div;
    if (results.length !== 0) {

        remplir();
        // Creation d'un nouvel objet qui va contenir en clee l'essence en valeur le tableau json correspondant.
        var tab_arbre = {};
        // 3 variable necessaire a l'execution de la suite (script asynchrone).
        var $injector = angular.injector(['ng']);
        var q = $injector.get('$q');
        var promise = q.all(null);
        // On met l'ensemble des url dans un tableau en utilisant notre tab_essence precedament remplis.
        var tab_url = new Array();
        for (var i = 0; i < tab_essence.length; i++) {
            tab_url.push('arbres/' + tab_essence[i] + '/index.json');
        }

        // ATTENTION script asynchrone aucun traitement ne doit etre fait.
        //A la fin on a remplit notre objet tab_arbre.
        angular.forEach(tab_url, function (url) {
            promise = promise.then(function () {
                return $http({
                    method: 'GET',
                    url: url
                }).then(function (res) {
                    // On ne veux que le nom de l'essence on l'extrer de la chaine de caractere.
                    var ess = url.substring(7, (url.length - 11));
                    tab_arbre[ess] = res.data;
                });
            });
        });
        // Fonction lancer quand tout le http est passer.
        promise.then(function () {

            // Creation d'un nouvel objet qui contiendra en clee l'id de la div et en valeur le tableau de stat remplis.
            var tab_d3 = new Object();
            for (var codePlacette in results_bis) {

                if (codePlacette === total) {
                    total_present = true;
                }

                // Variable pour le calcul du nombre d'arbre par placette.
                var nbrArbrePlacette = 0;
                for (essence in results_bis[codePlacette]) {
                    // Pour chaque essence dans un code placette on ajoute sont nombre d'abre au total d'abre de cette placette.
                    nbrArbrePlacette += results_bis[codePlacette][essence].length;
                }

                //On creer une nouvelle div : div codePlacette.
                var codePlacette_elem = document.createElement("div");
                // On y affecte l'id du codePlacette
                // ATTENTION ici on rajoute un _ car les placette sont des nombres et on supprime les espaces
                codePlacette_elem.id = '_' + codePlacette.replace(/\s/g, "");
                codePlacette_elem.className = 'stat_codePlacette';
                if (codePlacette === total) {
                    var t = document.createTextNode('Statistique globale' + ' - ' + nbrArbrePlacette + ' arbre(s)');
                } else {
                    var t = document.createTextNode('Placette ' + codePlacette + ' - ' + nbrArbrePlacette + ' arbre(s)');
                }

                // On ajoute le text creer a la div de la placette.
                codePlacette_elem.appendChild(t);
                for (var essence in results_bis[codePlacette]) {

                    //On recuperer la liste des arbres pour un codePlacette donner et une essence donner.
                    var listeArbres = results_bis[codePlacette][essence];
                    // On cré une nouvelle div : div essence.
                    var essence_elem = document.createElement("div");
                    // On y affecte l'id concatenation du codeplacette et de l'essence
                    essence_elem.id = '_' + codePlacette.replace(/\s/g, "") + essence.replace(/\s/g, "");
                    essence_elem.className = 'stat_essence';
                    var t = document.createTextNode(essence + ' - ' + listeArbres.length + ' arbre(s)');
                    essence_elem.appendChild(t);
                    // On affecte la div essence a la placette.
                    codePlacette_elem.appendChild(essence_elem);
                    // Creation d'une variable tableau qui contiendra les stat.
                    var chartData = new Array();
                    // On remplis notre tableau de stat avec les entrer et les valeurs a 0.
                    for (var ess in tab_arbre) {
                        if (ess === essence) {
                            var data = tab_arbre[ess];
                            for (var prop in data) {
                                // On parcoure notre tableau data et on ne recupere que les resultats qui on un type (Sa, R ....).
                                if (typeof data[prop].type !== "undefined") {
                                   chartData.push({label: data[prop].type + " - " + data[prop].etat,
                                        value:0,key:data[prop].type, color: data[prop].couleur});
                                }
                            }
                            break;
                        }
                    }

                    // On parcoure listeArbres remplit precedament et on incremente la valeur de chartData correspondant.
                    for (var i = 0; i < listeArbres.length; i++) {
                        // On recupere la derniere valeur de l'historique (historique.length -1)
                        var nbResult = listeArbres[i].historique[listeArbres[i].historique.length - 1];
                        for (var j = 0; j < chartData.length; j++) {
                            // On incremente la valeur correspondant au type d'arbre
                             if (chartData[j].label === data[nbResult].type + " - " + data[nbResult].etat) {
                                chartData[j].value++;
                                break;
                            }
                        }
                    }

                    // On remplis notre objet d3 qui servira a la generation des graphiques.
                    var cle = '_' + codePlacette.replace(/\s/g, "") + essence.replace(/\s/g, "");
                    tab_d3[cle] = chartData;
                }
                if (codePlacette === total) {
                    total_div = codePlacette_elem;
                }
                if (total && codePlacette !== total) {
                    document.getElementById("chart").insertBefore(codePlacette_elem, total_div);
                } else {
                    // On affecte l'id du codePlacette a la div de la vue.
                    document.getElementById("chart").appendChild(codePlacette_elem);
                }

            }

            // ATTENTION script asynchrone.
            // On parcourt notre objet d3 et on creer un nouveau graphique a chaque fois.
            // Celui ci est associer a la div essence de la div codePlacette.
            angular.forEach(tab_d3, function (value, key) {
                //create pie chart
                nv.addGraph(function () {
                    var chart = nv.models.pieChart()
                            .x(function (d) {
                                return d.label;
                            })
                            .y(function (d) {
                                return d.value;
                            })
                            .color(function (d) {
                                return d.color;
                            })
                            .showLabels(true)
                            .legendPosition("right")
                            .margin({"left":0,"right":0,"top":5,"bottom":5})
                            .labelType(function(d, i){ return d.data.key; });
                    d3.select("#" + key)
                            .append("svg")
                            .datum(value)
                            .transition().duration(350)
                            .call(chart);
                    return chart;
                });
            });
        });
    } else {
        var message = document.createTextNode("Aucune observations à analyser.");
        document.getElementById('chart').appendChild(message);
    }

    // Fonction qui sert a remplir nos tableau de donner avant traitement
    function remplir() {
        for (var i = 0; i < results.length; i++) {
            // Creation et initialisation d'une variable bon qui sert a verifier si le codePlacette est deja present
            var bon = false;
            // Variable qui servira a savoir si l'essence existe deja pour notre tab_essence.
            var essence_bon = false;
            for (var codePlacette in results_bis) {
                // Creation et initialisation d'une variable bon_2 qui sert a verifier si l'essence est deja presente dans notre results_bis.
                var bon_2 = false;
                essence_bon = false;
                //Si on a deja ce codePlacette
                if (codePlacette === results[i].codePlacette) {
                    for (var essence in results_bis[codePlacette]) {
                        // Si on a deja cette essence
                        if (essence === results[i].essence) {
                            // Alors on ajoute la valeur.
                            results_bis[codePlacette][essence].push(results[i]);
                            // La placette existe deja.
                            bon = true;
                            // l'essence existe deja.
                            bon_2 = true;
                            for (var j = 0; j < tab_essence.length; j++) {
                                if (tab_essence[j] === results[i].essence) {
                                    essence_bon = true;
                                    break;
                                }
                            }
                            // Si l'essence n'existe pas encore dans tab_essence.
                            if (!essence_bon) {
                                tab_essence.push(results[i].essence);
                            }
                            break;
                        }
                    }
                    //Ici on traite le cas de l'essence qui n'existe pas
                    if (!bon_2) {
                        results_bis[codePlacette][results[i].essence] = new Array();
                        results_bis[codePlacette][results[i].essence].push(results[i]);
                        bon = true;
                        for (var j = 0; j < tab_essence.length; j++) {
                            if (tab_essence[j] === results[i].essence) {
                                essence_bon = true;
                                break;
                            }
                        }
                        if (!essence_bon) {
                            tab_essence.push(results[i].essence);
                        }
                        break;
                    }
                    break;
                }
            }
            // Ici on traite le cas du codePlacette qui n'existe pas.
            if (!bon) {
                // On affecte un nouvel objet a notre entrer de codePlacette ( exemple : results_bis[test])
                results_bis[results[i].codePlacette] = new Object();
                // On affecte un nouveau tableau a notre objet precedament creer (exemple : results_bis[test][sapin])
                results_bis[results[i].codePlacette][results[i].essence] = new Array();
                // On ajoute le results courant dans l'essence correspondant
                results_bis[results[i].codePlacette][results[i].essence].push(results[i]);
                // On verifie qu'on a deja cette essence dans notre tab_essence.
                for (var j = 0; j < tab_essence.length; j++) {
                    if (tab_essence[j] === results[i].essence) {
                        essence_bon = true;
                        break;
                    }
                }
                // Si on a pas l'essence alors on l'ajoute au tableau.
                if (!essence_bon) {
                    tab_essence.push(results[i].essence);
                }
            }
        }

        // On rajoute l'entrée pour les statistiques globales
        var d = new Date();
        total = 'total' + d.getTime();
        results_bis[total] = new Object();
        for (var i = 0; i < results.length; i++) {
            // Creation et initialisation d'une variable bon_2 qui sert a verifier si l'essence est deja presente dans notre results_bis[total].
            var bon_2 = false;
            for (var essence in results_bis[total]) {
                if (essence === results[i].essence) {
                    results_bis[total][essence].push(results[i]);
                    // l'essence existe deja.
                    bon_2 = true;
                }
            }
            // Si l'essence n'existe pas encore
            if (!bon_2) {
                results_bis[total][results[i].essence] = new Array();
                results_bis[total][results[i].essence].push(results[i]);
            }
        }
    }
});