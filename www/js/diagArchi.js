/*
 * Fichier qui contient les différentes route de notre application
 */
"use strict";

var diagArchi = angular.module('diagArchi', ['ngSanitize', 'ngRoute', 'ngTouch', 'ui.bootstrap', 'angular-carousel', 'ksSwiper']);

diagArchi.config(function ($routeProvider) {
    $routeProvider
            .when('/', {
                templateUrl: 'views/home.html',
                controller: 'HomeController'
            })
            .when('/questions', {
                templateUrl: 'views/question.html',
                controller: 'QuestionController'
            })
            .when('/observations', {
                templateUrl: 'views/observations.html',
                controller: 'ObservationsController'
            })
            .when('/resultat', {
                templateUrl: 'views/result.html',
                controller: 'ResultController'
            })
            .when('/save', {
                templateUrl: 'views/save.html',
                controller: 'SaveController'
            })
            .when('/geolocalisation', {
                templateUrl: 'views/geolocalisation.html',
                controller: 'GeolocalisationController'
            })
            .when('/csv', {
                templateUrl: 'views/csv.html',
                controller: 'CsvController'
            })
            .when('/stats', {
                templateUrl: 'views/stats.html',
                controller: 'StatsController'
            })
            .when('/photo', {
                templateUrl: 'views/photo.html',
                controller: 'PhotoController'
            })
            .when('/ml', {
                templateUrl: 'views/ml.html',
                controller: 'MlController'
            })
            .when('/contact', {
                templateUrl: 'views/contact.html',
                controller: 'ContactController'
            })
            .otherwise({redirectTo: '/'});
});