/* 
 * Creation and management of modals
 */
"use strict";

var diagArchi = angular.module('diagArchi');
diagArchi.service('Modal', function ($http) {

    var glossaire;
    
    this.getGlossaire = function(){
        $http({method: 'GET', url: 'glossaire/glossaire.json'}).success(function (data, status, headers, config) {
            localStorage.setItem("glossaire", JSON.stringify(data));
            });
        // Variable pour le glossaire
        glossaire = JSON.parse(localStorage.getItem("glossaire"));
    };

    this.showHelp = function (arbre)
    {
        new Modal("Aide", null, "arbres/" + arbre + "/help.png", true).Show();
    };

    function showImage(title, image)
    {
        Modal.Clean();
        new Modal(title, null, "img/" + image + ".png").Show();
    }

    this.glossaireAffiche = function (mot) {
        function alertDismissed() {
        }

        var image = null;
        if (glossaire[mot].image !== null)
            image = "glossaire/" + glossaire[mot].image;

        // Boite de dialogue qui affichera la definition
        new Modal(mot, glossaire[mot].definition, image).Show();
    };



    var Modal = function (title, body, img, help) {
        var title = title;
        var body = body;
        var img = img;
        var help = help || false;
        
        var GetmodalStructure = function () {
            var that = Modal;
            that.Id = Modal.Id = Math.random();
            var modalString = "<div class='modal fade' name='dynamiccustommodal' id='"
                    + that.Id
                    + "' tabindex='-1' role='dialog'><div class='modal-dialog'> <div class='modal-content'>"
                    + "<div class='modal-header'>"
                    + "<button type='button' class='close modal-white-close' data-dismiss='modal'>"
                    + "<span aria-hidden='true'>&times;</span></button><h4 class='modal-title'>"
                    + title +
                    "</h4></div><div class='modal-body'> ";
            if (body !== null)
                modalString += body;
            if (img !== null) {
                modalString += "<img class='img-responsive' src='" + img + "'";
                if (help)
                    modalString += " id='img'";
                modalString += ">";
            }

            modalString += "</div><div class='modal-footer bg-default'> "
                    + "<button type='button' class='btn btn-primary'"
                    + "data-dismiss='modal'>Ok</button></div>"
                    + "</div></div></div></div></div>";

            return modalString;

        }();
        Modal.Clean = function ()
        {
            var modal = $(document.getElementsByName("dynamiccustommodal"));
            if (modal.length > 0)
            {
                modal.modal('hide');
                document.body.removeChild(modal[0]);
            }
        };
        this.Show = function () {
            Modal.Clean();
            document.body.appendChild($(GetmodalStructure)[0]);
            var modal = $(document.getElementById(Modal.Id));
            modal.modal('show');
        };
    };

});


