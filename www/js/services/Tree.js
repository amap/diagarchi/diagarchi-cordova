/*
 * Ce fichier sert a gerer les arbres.
 */
"use strict";

var diagArchi = angular.module('diagArchi');
diagArchi.service('Tree', function ($location) {

    // Constructeur
    this.newTree = function (data, type, args, update) {
        this.data = data;
        // Ajout du type d'essence
        this.type = type;
        this.version = data && data.meta ? data.meta.version : null;

        this.history = [];
        // Ici on rajoute un tableau  pour sauvegarder les reponses.
        this.cheminement = [];
        // default value for csv export
        this.observateur = null;
        this.secondObs = null;
        this.numArbre = null;
        this.codePlacette = null;
        this.longitude = null;
        this.latitude = null;
        // Ajout d'une liste de photo
        this.listPhoto = [];
        // Ajout du type d'arbre
        this.r_type = null;
        //observation effectuée ?
        this.effectue = false;
        

        this.isBeingUpdated = update || false;

        // check args
        if (typeof args !== 'undefined')
        {
            this.history = args.history;
            this.cheminement = args.cheminement;
            this.observateur = args.observateur;
            this.secondObs = args.secondObs;
            this.numArbre = parseInt(args.numArbre, 10);
            this.codePlacette = args.codePlacette;
            this.longitude = args.longitude;
            this.latitude = args.latitude;
            this.listPhoto = args.listPhoto;
            this.r_type = args.r_type;
        }
    };

    // Fonction pour aller al a question suivante
    this.goToNextQuestion = function (rep) {
        var nextQuestionNb = this.data[this.history[this.history.length - 1]].reponse[rep];

        if (this.data[nextQuestionNb].hasOwnProperty("question")) {
            // Rajout du tableau cheminement dans l'url.
            $location.url('questions?type=' + this.type + '&nb=' + nextQuestionNb + '&history=' + JSON.stringify(this.history) + '&cheminement=' + JSON.stringify(this.cheminement));
        } else {
            // Rajout du tableau cheminement dans l'url.
            $location.url('resultat?type=' + this.type + '&nb=' + nextQuestionNb + '&history=' + JSON.stringify(this.history) + '&cheminement=' + JSON.stringify(this.cheminement));
        }
    };

    // Fonction pour aller a une question specifique
    this.goToQuestion = function (nb) {
        // Rajout du tableau cheminement dans l'url.
        $location.url('questions?type=' + this.type + '&nb=' + nb + '&history=' + JSON.stringify(this.history) + '&cheminement=' + JSON.stringify(this.cheminement));
    };

    // Fonction pour aller au resultat
    this.goToResult = function () {
        // Rajout du tableau cheminement dans l'url.
        $location.url('resultat?type=' + this.type + '&nb=' + this.history.pop() + '&history=' + JSON.stringify(this.history) + '&cheminement=' + JSON.stringify(this.cheminement));
    };

    // Fonction pour aller a la sauvegarde
    this.goToSave = function () {
        $location.url('save?type=' + this.type + '&history=' + JSON.stringify(this.history));
    };

    // Fonction pour modifier un arbre : clic sur une question de l'historique. On supprime les valeur qui ne nous serve plus.
    this.rewind = function (nb) {
        for (var i = 0; i < this.history.length; i++) {
            if (this.history[i] === nb) {
                this.history.splice(i, Number.MAX_VALUE);
                this.cheminement.splice(i, Number.MAX_VALUE);
            }
        }
        this.goToQuestion(nb);
    };

    this.toString = function () {
        return "Type: " + this.type + "\n" + "History: " + this.history;
    };
});

