/*
 * Ce fichier sert a transformer les données en csv ou le scv en donnée.
 */
"use strict";

// This will parse a delimited string into an array of
// arrays. The default delimiter is the comma, but this
// can be overriden in the second argument.
function CSVToArray(strData, strDelimiter) {
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ",");

    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp(
            (
                    // Delimiters.
                    "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
                    // Quoted fields.
                    "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
                    // Standard fields.
                    "([^\"\\" + strDelimiter + "\\r\\n]*))"
                    ),
            "gi"
            );


    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];

    // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;

    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec(strData)) {

        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[ 1 ];

        // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (
                strMatchedDelimiter.length &&
                (strMatchedDelimiter !== strDelimiter)
                ) {

            // Since we have reached a new row of data,
            // add an empty row to our data array.
            arrData.push([]);

        }


        // Now that we have our delimiter out of the way,
        // let's check to see which kind of value we
        // captured (quoted or unquoted).
        var strMatchedValue;
        if (arrMatches[ 2 ]) {

            // We found a quoted value. When we capture
            // this value, unescape any double quotes.
            strMatchedValue = arrMatches[ 2 ].replace(
                    new RegExp("\"\"", "g"),
                    "\""
                    );

        } else {

            // We found a non-quoted value.
            strMatchedValue = arrMatches[ 3 ];

        }


        // Now that we have our value string, let's add
        // it to the data array.
        arrData[ arrData.length - 1 ].push(strMatchedValue);
    }

    // Return the parsed data.
    return arrData;
}

var diagArchi = angular.module('diagArchi');
diagArchi.service('CSV', function () {

    // Fonction pour l'export
    this.export = function (filename) {

        if (JSON.parse(localStorage.getItem("results")) !== null) {
             window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, gotFS, fail);
        
        } else {
            alert("Aucun arbre à exporter.");
        }

        function gotFS(fileSystem) {
            fileSystem.getFile(filename + ".csv", {create: true, exclusive: false}, gotFileEntry, fail);
        }

        function gotFileEntry(fileEntry) {
            fileEntry.createWriter(gotFileWriter, fail);
        }

        function gotFileWriter(writer) {
            writer.onwriteend = function (evt) {
                alert("Liste exportée dans " + filename + ".csv");
            };

            var results = JSON.parse(localStorage.getItem("results"));
            var header = "Observateur\tSecondObs\tdateObservation\tnumArbre\tEssence\tcodePlacette\tlongitude\tlatitude\thistorique\tcheminement\ttype\tversion\tphoto\r\n";
            var body = "";
            for (var i = 0; i < results.length; i++) {
                body += results[i].observateur;
                body += "\t";
                body += results[i].secondObs;
                body += "\t";
                body += results[i].dateObservation;
                body += "\t";
                body += results[i].numArbre;
                body += "\t";
                body += results[i].essence;
                body += "\t";
                body += results[i].codePlacette;
                body += "\t";
                body += results[i].longitude;
                body += "\t";
                body += results[i].latitude;

                // Add history
                body += "\t";
                for (var j = 0; j < results[i].historique.length; j++) {
                    body += results[i].historique[j];
                    if (j < results[i].historique.length - 1) {
                        body += "/";
                    }
                }

                // ajout cheminement
                body += "\t";
                for (var j = 0; j < results[i].cheminement.length; j++) {
                    body += results[i].cheminement[j];
                    if (j < results[i].cheminement.length - 1) {
                        body += "/";
                    }
                }

                // Ajout du type
                body += "\t";
                body += results[i].r_type;

                // version
                body += "\t";
                body += results[i].version;

                // Modifier pour afficher la liste des nom des photot dans l'export.
                if (results[i].listPhoto.length !== 0) {
                    body += "\t";
                    for (var j = 0; j < results[i].listPhoto.length; j++) {
                        body += results[i].listPhoto[j].name;
                        if (j < results[i].listPhoto.length - 1) {
                            body += "/";
                        }
                    }
                }


                // EOF
                if (i < results.length - 1) {
                    body += "\r\n";
                }
            }
            writer.write(header + body);
        }

        function fail(error) {
            alert("Un problème est survenu lors de l'exportation des données. Code d'erreur : " + error.code);
        }
    };

    // Fonction pour l'import.
    this.import = function (filename, callback) {

        window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, gotFS, fail);

        function gotFS(fileSystem) {
            fileSystem.getFile(filename + ".csv", {}, gotFileEntry, fail);
        }

        function gotFileEntry(fileEntry) {
            fileEntry.file(gotFileReader, fail);
        }

        function gotFileReader(file) {
            var reader = new FileReader();

            reader.onloadend = function (e) {
                var array = new CSVToArray(e.target.result);
                array.splice(0, 1); // enleve la premiere ligne

                var goodArray = [];
                for (var i = 0; i < array.length; i++) {
                    var array_bis = array[i][0].split('\t');
                    // import de date au format toString
                    //var d = new Date(array_bis[1]);

                    goodArray.push({
                        observateur: array_bis[0],
                        //dateObservation: d.toLocaleDateString(),
                        seconObs: array_bis[1],
                        dateObservation: array_bis[2],
                        numArbre: Number(array_bis[3]),
                        essence: array_bis[4],
                        codePlacette: array_bis[5],
                        longitude: array_bis[6] === "null" ? null : array_bis[5],
                        latitude: array_bis[7] === "null" ? null : array_bis[6],
                        historique: array_bis[8].split("/"),
                        cheminement: array_bis[9].split("/"),
                        r_type: array_bis[10],
                        version: array_bis[11],
                        listPhoto: []
                    });
                    
                }

                callback(goodArray);
            };

            reader.readAsText(file);
        }

        function fail(error) {
            alert("Un problème est survenu lors de l'importation des données. Code d'erreur : " + error.code + ' message : ' + error.message);
        }
    };
});

