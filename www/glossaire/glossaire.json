{
        "A1, A2, A3 ": {
            "definition": "L'ordre de ramification éfinit la topologie d’un système ramifié. L’axe issu de la graine est d’ordre 1 et porte un axe d’ordre 2, qui lui-même porte un axe d’ordre 3 et ainsi de suite.",
            "image":null
        },
        "Architecture végétale": {
            "definition": "Une série de caractères morphologiques exprimée par une plante au cours de son développement (ontogénie) mais aussi une méthode d'étude de l'organisation spatio-temporelle de la structure végétale. A l'origine, l’architecture a été développée pour comprendre et caractériser l'élaboration de la forme des arbres.",
            "image": null
        },
        "Arbre dominant ou co-dominant": {
            "definition": "",
            "image":"dominants.png"
        },
        "Arbre résilient": {
            "definition": "Arbre retrouvant une dynamique architecturale normale",
            "image":null
        },
        "Arbre sain": {
            "definition": "Arbre dont l’architecture est conforme à son stade de développement",
            "image":null
        },
        "Arbre stressé": {
            "definition": "Arbre dont l’architecture s’écarte de la norme",
            "image":null
        },
        "Arbre mort": {
            "definition": "Arbre dont le cambium du tronc est mort à 1,3 m de hauteur",
            "image":null
        },
        "Branches mortes": {
            "definition": "",
            "image":"branche_morte.png"
        },
        "Chicots": {
            "definition": "Branche cassée de diamètre supérieur à 3 cm. Ne pas confondre « chicot » (plaie non recouverte par du bois) et « coude » (plaie recouverte).",
            "image":"chicot.png"
        },
        "Dépérissement irréversible": {
            "definition": "Arbre bloqué dans une situation architecturale de non retour à la normale.",
            "image":null
        },
        "Descente de cime": {
            "definition": "Arbre présentant une dynamique de construction d’un nouveau houppier sous la cime.",
            "image":null
        },
        "Draperies de rameaux": {
            "definition": "Séries successives de rameaux affaissés se relayant le long des branches pour maintenir une surface chlorophyllienne optimale. ",
            "image":"draperies_rameaux.png"
        },
        "2e houppier": {
            "definition": "Structure constituée de suppléants (parfois mélangés à des branches) hiérarchisés entre eux, certains étant dominants, d’autres dominés.",
            "image":null
        },
        "Flèche": {
            "definition": "Chez les résineux, partie sommitale du tronc comprenant les six derniers étages de branches. ",
            "image":null
        },
        "Fourche": {
            "definition": "Structure composée de deux axes de diamètre équivalent.",
            "image":null
        },
        "Suppléants": {
            "definition": "Structure issue d’un bourgeon qui ne s’est pas développé en branche et qui est donc resté en attente pendant plus d’un an, aussi appelé « rejet », « rameau épicormique » ou « suppléant ». Un suppléant se distingue d’une branche par les caractères suivants : insertion en retrait des structures mortes ou dépérissantes, diamètre basal plus petit que celui de l’axe porteur, angle d’insertion différent de celui d’une branche, aspect de l’écorce plus jeune, vigueur plus forte (sauf pour les suppléants agéotropes).",
            "image": "suppleant_1.png"
        },
        "Suppléants nombreux": {
            "definition": "Pour les Conifères, suppléants présents sur plus de 50 % des branches et les recouvrant sur plus du quart de leur longueur. Sur l’A1, les suppléants sont nombreux lorsqu’ils cachent la partie du tronc qui les porte. ",
            "image":null
        },
        "Orthotropes": {
            "definition": "(du grec « orthos » : droit et de « tropos » : direction) : suppléant à symétrie axiale ayant une direction de croissance verticale et reproduisant l’architecture entière de jeunes arbres.",
            "image": "suppleant_2.png"
        },
        "Agéotropes": {
            "definition": "(du grec « a » : sans, « géo » : terre et « tropos » : direction) : suppléant à croissance réduite, sans direction de croissance privilégiée et pouvant même pousser « la tête en bas ». ",
            "image": "suppleant_2.png"
        },
        "Houppier HC": {
            "definition": "Houppier Hors Concurrence qui exclut les zones inférieures et latérales soumises à la concurrence des voisins. Après une éclaircie, la limite du houppier HC change. Il est recommandé d’attendre 2 à 3 ans avant les premières notations ARCHI afin de laisser le temps aux suppléants de s’exprimer en pleine lumière.",
            "image":"houppier_hc.png"
        },
        "Mortalité anormale": {
            "definition":"",
            "image":"mortalite_anormale.png"
        },
        "Nombreux": {
            "definition": "Pour les Conifères, suppléants présents sur plus de 50 % des branches et les recouvrant sur plus du quart de leur longueur. Sur l’A1, les suppléants sont nombreux lorsqu’ils cachent la partie du tronc qui les porte. ",
            "image":null
        },
        "Normale": {
            "definition": "Contour quasi-pyramidal du système ramifié présentant un gradient de ramification depuis l’axe principal (A1) jusqu’aux rameaux.",
            "image":"ramification_normale.png"
        },
        "Ontogénie": {
            "definition": "Développement d’un organisme de la graine jusqu’à sa mort.",
            "image":null
        },
        "Plagiotropes": {
            "definition": "(du grec « plagios » : oblique) : suppléant à symétrie bilatérale ayant une direction de croissance horizontale à oblique et reproduisant l’architecture de jeunes branches.",
            "image": "suppleant_2.png"
        },
        "Ramification": {
            "definition": "Processus de production d'un axe feuillé à partir d'un autre. La ramification latérale résulte du développement d’un méristème à l'aisselle de la feuille.",
            "image":"ramification_normale.png"
        },
        "Ramification appauvrie": {
            "definition": "Contour colonnaire du système ramifié présentant un passage brutal de l’axe principal (A1) aux rameaux courts (cas le plus fréquent) ou une absence de ramification des branches (A2). Ne pas confondre ramification appauvrie et densité de la ramification. Cette dernière est parfois faible en raison d’une croissance forte entrainant un espacement des zones ramifiées.",
            "image" : "ramification_appauvrie.png"
        },
        "Réitération": {
            "definition": "Même si quelques arbres restent conformes à leur unité architecturale tout au long de leur vie, il est facile de constater dans la nature que, chez la plupart des espèces, l'arbre jeune, pyramidal, entièrement hiérarchisé autour d'un tronc unique, cède la place, à des âges plus avancés, à un arbre plus complexe dont la cime s'arrondit et dont le houppier se structure autour de nombreuses et puissantes branches maîtresses. Cette transformation résulte, le plus souvent, d'une duplication de l'unité architecturale au cours de l'ontogénie.",
            "image":null
        },
        "Type R3 et/ou R4": {
            "definition": "Système ramifié de type R3 est composé d’un axe principal qui portent des rameaux longs qui eux-mêmes portent des rameaux courts. Le type R4 a un ordre de ramification en plus. Le type R2 (branche portant uniquement des rameaux courts) ou R1 (pas de ramification) est une ramification appauvrie.",
            "image":null
        },
        "UA sommitales": {
            "definition": "Une Unité Architecturale (ou U.A.) est l’architecture élémentaire de l’arbre. La première est à l’origine du tronc, les suivantes dérivent les unes des autres par réitération et forment le houppier. Le long d’une branche maîtresse, chaque UA est délimitée par deux fourches successives. Les dernières UA sont les UA sommitales.",
            "image":"unite_sommitale.png"
        }
}
